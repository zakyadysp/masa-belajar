<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="stylesheet" href="css/style.css">
    <script src="js/style.js"></script>
    <title>Calculator T2</title>
</head>

<!-- create table -->

<body>
    <div class=title>GeeksforGeeks Calculator</div>
    <table>
        <tr>
            <td colspan="4"><input type="text" id="result" /></td>
            <!-- clr() function will call clr to clear all value -->

        </tr>
        <tr>
            <!-- create button and assign value to each button -->
            <!-- dis("1") will call function dis to display value -->
            <td><input type="button" value="1" onclick="dis('1')" /> </td>
            <td><input type="button" value="2" onclick="dis('2')" /> </td>
            <td><input type="button" value="3" onclick="dis('3')" /> </td>
            <td><input type="button" class="operator" value="+" onclick="dis('+')" /> </td>

        </tr>
        <tr>
            <td><input type="button" value="4" onclick="dis('4')" /> </td>
            <td><input type="button" value="5" onclick="dis('5')" /> </td>
            <td><input type="button" value="6" onclick="dis('6')" /> </td>
            <td><input type="button" class="operator" value="-" onclick="dis('-')" /> </td>
        </tr>
        <tr>
            <td><input type="button" value="7" onclick="dis('7')" /> </td>
            <td><input type="button" value="8" onclick="dis('8')" /> </td>
            <td><input type="button" value="9" onclick="dis('9')" /> </td>
            <td><input type="button" class="operator" value="x" onclick="dis('*')" /> </td>

        </tr>
        <tr>
            <td><input type="button" id="c" value="c" onclick="clr()" /> </td>
            <td><input type="button" value="0" onclick="dis('0')" /> </td>
            <td><input type="button" value="=" onclick="solve()" /> </td>
            <td><input type="button" class="operator" value="/" onclick="dis('/')" /> </td>
            <!-- solve function call function solve to evaluate value -->

        </tr>
    </table>
</body>

</html>