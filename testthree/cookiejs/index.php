<!DOCTYPE html>
<html lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>Untitled Document</title>
    <link href="amandastyle.css" rel="stylesheet" type="text/css" />
    <script src="js/func.js"></script>
</head>

<body>
    <div id="container">
        <div id="text">
            <p align="center"> Click a colour to change background</p>
            <p align="center"><a href="javascript:changeBG(0)">pink</a></p>
            <p align="center"><a href="javascript:changeBG(1)">blue</a></p>
            <p align="center"><a href="javascript:changeBG(2)">green</a></p>
            <p align="center"><a href="javascript:changeBG(3)">purple</a></p>
            <p align="center"><a href="javascript:changeBG(4)">white</a></p>
            <p align="center"><a href="javascript:changeBG(5)">gray</a></p>
        </div>
    </div>
</body>

</html>