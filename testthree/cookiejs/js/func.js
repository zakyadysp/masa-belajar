function createCookie(name, value, days) {
    if (days) {
        var date = new Date();
        date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
        var expires = "; expires=" + date.toGMTString();
    } else var expires = "";
    document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
    var nameEQ = name + "=";
    var ca = document.cookie.split(';');
    for (var i = 0; i < ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') c = c.substring(1, c.length);
        if (c.indexOf(nameEQ) == 0) return c.substring(nameEQ.length, c.length);
    }
    return null;
}

function eraseCookie(name) {
    createCookie(name, "", -1);
}

var backColor = new Array();

backColor[0] = '#FFCCFF';
backColor[1] = '#CCFFFF';
backColor[2] = '#99FFCC';
backColor[3] = '#CCCCFF';
backColor[4] = '#FFFFFF';
backColor[5] = '#CCCCCC';


function changeBG(whichColor) {
    document.body.style.backgroundColor = backColor[whichColor];
    createCookie('backColor', whichColor);
}

if (readCookie('backColor'))
    document.write('<style type="text/css">body {background-color: ' + backColor[readCookie("backColor")] +
        ';}<\/style>');